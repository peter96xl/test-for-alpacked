# Базовый образ Ubuntu
FROM ubuntu:bionic

MAINTAINER Peter

# Обновление системы, загрузка zabbix 5, запуск с конфигурацией по умолчанию, загрузка confd, создание директорий
RUN apt update -y && apt install curl -y && \
    curl -Lo /tmp/zabbix.tar.gz https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1%2Bbionic.tar.gz && \
    tar -C /tmp/ -xf /tmp/zabbix.tar.gz && \
    cd /tmp && mkdir zabbix && \
    (cd /tmp/zabbix && ./configure --enable-server --with-postgresql && make && make install) && \
    cp -aP /tmp/zabbix/database/postgresql/*.sql /usr/local/share/zabbix/ && \
    rm -r /tmp/* && \
    useradd --system zabbix && \
    curl -Lo /usr/bin/confd https://github.com/kelseyhightower/confd/releases/download/v0.16.0/confd-0.16.0-linux-amd64 && \
    chmod +x /usr/bin/confd && \
    mkdir -p /etc/confd/templates/ && \
    mkdir -p /etc/confd/conf.d/ && \

COPY *.toml /etc/confd/conf.d/

COPY *.tmpl /etc/confd/templates/

COPY entrypoint.sh /

RUN chmod +x /entrypoint.sh && \
    && apt-get clean -y \
    && echo "==== Done ==="

EXPOSE 80 
