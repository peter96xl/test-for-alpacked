# Test For Alpacked

В данном Dockerfile были попытки запускать zabbix сервер. К сожалению все попытку увенчались неудачей. Релиз zabbix брался с оф сайта https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/ . В последние время при билде все время нарываюсь на ошибку "/bin/sh: 1: ./configure: not found". Для решения данной проблемы использовал эту статтю https://stackoverflow.com/questions/44451696/bin-sh-1-configure-not-found-dockerfile . Но сколько не пытался результат все время был отрицательный...

# Последовательность действий 

1. Создать репозиторий Gitlab (были попытки развернуть сервер Gitlab на GCP, но к сожалению там я столкнулся с проблемой самоподписаного сертификата, для решения проблемы воспользовался туториалом https://www.youtube.com/watch?v=sTDVsMUegL8&list=LLeBpne9mDufwltB5FYNVuQQ&index=2&t=712s , но по непонятным причанам гугл долго одобряд сертификат и я решил не тратить время и воспользовался Gitlab Trial)
2. Добавление в variables данные с docker hub
3. Написание Dockerfile (неудачно)
